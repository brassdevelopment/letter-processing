int fc, num = 6000;
ArrayList pixelPointList;
boolean save = false;
float scal, theta;
PGraphics letter;
PFont font;
String characterLetter = "E";
color dotCol = color(255, 174, 0);
color lineCol = color(255, 222, 0);
int numAlp = 0;

void setup() {
    background(20);
    size(600, 600);
    letter = createGraphics(width, height);
    font = loadFont("Arial-Black-48.vlw");
    pixelPointList = new ArrayList();
    createStuff();
    frameRate(24);
}

void draw() {
    background(20);
    for (int i = 0; i < pixelPointList.size(); i++) {
        PixelPoint mb = (PixelPoint) pixelPointList.get(i);
        mb.run();
    }

    theta += .0523;
}

void createStuff() {
    pixelPointList.clear();

    letter.beginDraw();
    letter.noStroke();
    letter.background(255);
    letter.fill(0);
    letter.textFont(font, width);
    letter.textAlign(CENTER);
    letter.text(characterLetter, width / 2, height / 1.15);
    letter.endDraw();
    letter.loadPixels();
    float maxRadius = 150;
    for (int i = 0; i < num; i++) {
        int x = (int) random(width);
        int y = (int) random(height);
        color c = letter.get(x, y);
        //int c = letter.pixels[x+y*width];
        if (brightness(c) < 255) {
            PVector org = new PVector(x, y);
            float radius = random(5, maxRadius);
            PVector loc = new PVector(org.x + radius, org.y + radius);
            float offSet = random(TWO_PI);
            int dir = 1;
            float r = random(1);
            color lineColor = color(250);
            color dotColor = color(255);
            if (i % 5 == 0) {
                lineColor = lineCol;
                dotColor = dotCol;
            }
            if (r > .5) dir = -1;
            PixelPoint myPixelPoint = new PixelPoint(org, loc, radius, dir, offSet, 0, lineColor, dotColor);
            pixelPointList.add(myPixelPoint);
        }
    }
}


class PixelPoint {

    PVector org, loc;
    float sz = 2;
    float radius, offSet, a;
    int s, dir, countC, d = 20;
    boolean[] connection = new boolean[num];
    int pixelFrameCount;
    boolean goingBackwards = false;
    int switchCount = 0;
    color pxColor = dotCol;
    color linerColor = lineCol;

    PixelPoint(PVector _org, PVector _loc, float _radius, int _dir, float _offSet, int _pixelFrameCount, color _lineColor, color _dotColor) {
        org = _org;
        loc = _loc;
        radius = _radius;
        dir = _dir;
        offSet = _offSet;
        pixelFrameCount = _pixelFrameCount;
        linerColor = _lineColor;
        pxColor = _dotColor;
    }

    void run() {
        display();
        move();
        lineBetween();
    }

    void move() {
        if (this.radius > 5 && this.goingBackwards == false) {
            this.radius--;
        } else if (this.goingBackwards == true) {
            this.radius++;
        }
        loc.x = org.x + sin(theta * dir + offSet) * radius;
        loc.y = org.y + cos(theta * dir + offSet) * radius;
    }

    color shiftHue(color hueColor, int inc) {
        print("Adjusting colour  \n");
        final color c = hueColor;
        return color((inc + (int) hue(c)) % 360, saturation(c), brightness(c));
    }

    void lineBetween() {
        countC = 1;
        for (int i = 0; i < pixelPointList.size(); i++) {
            PixelPoint other = (PixelPoint) pixelPointList.get(i);
            float distance = loc.dist(other.loc);
            if (distance > 0 && distance < d) {
                a = map(countC, 0, 10, 10, 255);
                stroke(this.linerColor, a);
                line(loc.x, loc.y, other.loc.x, other.loc.y);
                connection[i] = true;
            } else {
                connection[i] = false;
            }
        }
        for (int i = 0; i < pixelPointList.size(); i++) {
            if (connection[i]) countC++;
        }
    }

    void display() {
        noStroke();
        fill(this.pxColor);
        ellipse(loc.x, loc.y, sz, sz);
    }
}