# Processing Swarm Effect #

Desktop and web Processing files for swarm letter effect. 

### Usage ###

* Run the web/index.html on a functioning web server
* Run desktop/sketch_letter/sketch_letter.pde in the desktop Processing application

### Owner ###

Written by Tom Bennett